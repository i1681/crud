   <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pagina</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<body>
    <div class="container">

    <?php if(session('mensaje')){?>

<div class="alert alert-danger" role="alert">
    <?php
    echo session('mensaje');

    ?>
</div>
<?php  } ?>

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Ingresa tus datos</h5>
            <p class="card-text">
                
            <form method="post" action="<?=site_url('/ingresar')?>" enctype="multipart/form-data">


                <div class="form-group">
                    <label for="usuario">Usuario</label>
                    <input id="usuario" class="form-control" type="text" name="usuario">

                </div>

                <div class="form-group">
                    <label for="pass">Contraseña</label>
                    <input id="pass" class="form-control" type="password" name="pass">
                </div>
                    
                <button class="btn btn-success" type="submit">Ingresar</button>
            </form>

            </p>
        </div>
    </div>
    </div>
</body>
</html>

   

