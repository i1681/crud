<?=$cabecera;?>

<br />
<a class="btn btn-success" href="<?=base_url('usuarios/crear')?>"> Crear Usuario</a>
<a class="btn btn-danger" href="<?=base_url('/')?>"> Salir</a>
<br />
<br />
        <table class="table table-light">
            <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Usuario</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach($usuarios as $usuario): ?>
                <tr>
                    <td><?=$usuario['id']; ?></td>
                    <td><?=$usuario['nombre']; ?></td>
                    <td><?=$usuario['usuario']; ?></td>
                    <td>
                   
                    <a href="<?=base_url('editar/'.$usuario['id']); ?>" class="btn btn-info" type="button">Editar</button>
                    <a href="<?=base_url('borrar/'.$usuario['id']); ?>" class="btn btn-danger" type="button">Borrar</button>
                    
                    </td>
                </tr>
            
            <?php endforeach; ?>

            </tbody>
        </table>
<?=$pie;?>