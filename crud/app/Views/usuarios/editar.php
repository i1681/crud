<?=$cabecera;?>

<div class="card">
        <div class="card-body">
            <h5 class="card-title">Editar Usuario</h5>
            <p class="card-text">
                
            <form method="post" action="<?=site_url('/actualizar')?>" enctype="multipart/form-data">


            <input type="hidden" name="id" value="<?=$usuario['id'];?>">
            <input type="hidden" name="pass" value="<?=$usuario['pass'];?>">
            
            <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input id="nombre" value="<?=$usuario['nombre'];?>" class="form-control" type="text" name="nombre">

                </div>
                <div class="form-group">
                    <label for="usuario">Usuario</label>
                    <input id="usuario" value="<?=$usuario['usuario'];?>" class="form-control" type="text" name="usuario">

                </div>
                    
                <button class="btn btn-success" type="submit">Actualizar</button>
                <a href="<?=base_url('listar');?>" class="btn btn-info">Cancelar</a>
            </form>
    
            </p>
        </div>
    </div>

<?=$pie;?>