<?=$cabecera?>
   
<?php if(session('mensaje')){?>

<div class="alert alert-danger" role="alert">
    <?php
    echo session('mensaje');

    ?>
</div>
<?php 
    }
?>

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Ingresar datos del Usuario</h5>
            <p class="card-text">
                
            <form method="post" action="<?=site_url('/guardar')?>" enctype="multipart/form-data">

            <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input id="nombre" value= "<?=old('nombre')?>" class="form-control" type="text" name="nombre">

                </div>

                <div class="form-group">
                    <label for="usuario">Usuario</label>
                    <input id="usuario" value= "<?=old('usuario')?>" class="form-control" type="text" name="usuario">

                </div>

                <div class="form-group">
                    <label for="pass">Contraseña</label>
                    <input id="pass" class="form-control" type="password" name="pass">
                </div>
                    
                <button class="btn btn-success" type="submit">Guardar</button>
                <a href="<?=base_url('listar');?>" class="btn btn-info">Cancelar</a>
            </form>

            </p>
        </div>
    </div>


   

<?=$pie?>