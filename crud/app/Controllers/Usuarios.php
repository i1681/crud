<?php 
namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Usuario;
class Usuarios extends Controller{

    public function index(){

        $usuario=new Usuario();
        $datos['usuarios']=$usuario->orderBy('id','ASC')->findAll();

        $datos['cabecera']=view('template/cabecera');
        $datos['pie']=view('template/piepagina');

        return view('usuarios/listar',$datos);

    }
  
    public function crear(){

        $datos['cabecera']=view('template/cabecera');
        $datos['pie']=view('template/piepagina');

        return view('usuarios/crear',$datos);

    }

    public function guardar(){

        $usuario=new Usuario();

        $validacion = $this->validate([
            'nombre' => 'required|min_length[3]',
            'usuario' => 'required|min_length[4]',
            'pass' => 'required|min_length[4]',
        ]);
        
        if(!$validacion){
            $session = session();
            $session ->setFlashdata('mensaje','revise la información');

            return redirect()->back()->withInput();
        }

        $datos=[
            'nombre'=>$this->request->getVar('nombre'),
            'usuario'=>$this->request->getVar('usuario'),
            'pass' => password_hash($this->request->getVar('pass'),PASSWORD_DEFAULT)

        ];

        $usuario->insert($datos);

        return $this->response->redirect(site_url('/listar'));

    }

    public function borrar($id=null){

        $usuario=new Usuario();
        $datosUsuario=$usuario->where('id',$id)->first();

        $usuario->where('id',$id)->delete($id);

        return $this->response->redirect(site_url('/listar'));

    }

    public function editar($id=null){
        
        $usuario=new Usuario();
        $datos['usuario']=$usuario->where('id',$id)->first();
        
        $datos['cabecera']=view('template/cabecera');
        $datos['pie']=view('template/piepagina');

        return view('usuarios/editar', $datos);


    }

    public function actualizar(){

        $usuario=new Usuario();


        $validacion = $this->validate([
            'nombre' => 'required|min_length[3]',
            'usuario' => 'required|min_length[4]',
        ]);
        
        if(!$validacion){
            $session = session();
            $session ->setFlashdata('mensaje','revise la información');

            return redirect()->back()->withInput();
        }

        $datos=[
            'nombre'=>$this->request->getVar('nombre'),
            'usuario'=>$this->request->getVar('usuario'),

        ];
        $id=$this->request->getVar('id');
        $usuario->update($id,$datos);

        return $this->response->redirect(site_url('/listar'));

    }

    public function ingresar(){

       //echo "entre a la funcion ingresar";
        $usuario=new Usuario();
        $validacion = $this->validate([
            'usuario' => 'required',
            'pass' => 'required',
        ]);
        
        if(!$validacion){
            $session = session();
            $session ->setFlashdata('mensaje','revise la información');

            return redirect()->back()->withInput();

        } else {
            $datos=[
                'usuario'=>$this->request->getVar('usuario'),
                'pass'=>$this->request->getVar('pass'),
                ];
        
            $password=$this->request->getVar('pass');
            $datosUsuario['usuario']=$usuario->select('usuario')->where('usuario',$datos['usuario'])->first();
            $datosUsuario['pass']=$usuario->select('pass')->where('usuario',$datos['usuario'])->first();
            echo "usuario desde base de datos".":";
            //print_r($datosUsuario['usuario']['usuario']);echo"<br>";

            if (isset($datosUsuario['pass']['pass'])){
                if (password_verify($password,$datosUsuario['pass']['pass'])){

                    echo "usuario logueado";echo"<br>";
                    return $this->response->redirect(site_url('/listar'));
                    
                } else {
                    $session = session();
                    $session ->setFlashdata('mensaje','Usuario o Contraseña incorrecto');

                    return redirect()->back()->withInput();
                }

            } else {
                    $session = session();
                    $session ->setFlashdata('mensaje','Usuario o Contraseña incorrecto2');
                    return redirect()->back()->withInput();
            }        

        }
        

        
       
       

    }
}